# ATOM is meeting on Tuesday, 7th November, 6:30pm, at Galvanize!

Join us for a highly participative discussion ! Alando Ballantyne will be taking us through the historical foundations of modern-day deep learning. Here's his abstract :

In 1969 Marvin Minsky famously showed that a single perceptron wasn't capable of learning the XOR function. In this discussion, we will gain an intuitive understanding of Minsky's "And / Or" Theorem, as well as examine how a modern feedforward network with one hidden layer composed of two units (perceptrons) is capable of overcoming the limitations outlined in Minsky's theorem, and can thereby learn the XOR function.

If you'd like to read up a little, [this article](https://beamandrew.github.io/deeplearning/2017/02/23/deep_learning_101_part1.html) provides a good history, and the context is fascinating !

Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.

We're kindly hosted by Galvanize. Thank you !
